# MassTransit example

This project requires Docker to be installed.
To start up the services, run the following command from this directory:

```
docker-compose up --build
```

`Messaging.Az` project should be running on port `5004`. Try sending http request:

```
url: http://localhost:5004/send
method: POST
body:
{
    "text": "Some text",
    "throwException": false
}
```

Alternatively, open swagger page on: `http://localhost:5004/swagger/index.html`