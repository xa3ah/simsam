﻿using MassTransit;
using Messaging.Shared;

namespace Messaging.Buki;

public class CommandMessageConsumer : IConsumer<CommandMessage>
{
    private readonly IBus _bus;
    public CommandMessageConsumer(IBus bus) => _bus = bus;
    
    public async Task Consume(ConsumeContext<CommandMessage> context)
    {
        Console.WriteLine($"Consuming command {context.Message.Id}");
        
        var @event = new EventMessage
        {
            Id = context.Message.Id,
            Text = $"Executed {context.Message.Command}",
            ThrowException = context.Message.ThrowException
        };

        await _bus.Publish(@event);
    }
}
