﻿using MassTransit;

namespace Messaging.Buki;

public static class ConfigureMassTransitService
{
    public static void ConfigureMassTransit(this IServiceCollection services)
    {
        services.AddMassTransit(c =>
        {
            c.AddConsumer<CommandMessageConsumer>();

            c.UsingRabbitMq((ctx, cfg) =>
            {
                cfg.Host(Environment.GetEnvironmentVariable("RABBITMQ_HOSTNAME") ?? "localhost", 5672, "/", h =>
                {
                    h.Username(Environment.GetEnvironmentVariable("RABBITMQ_DEFAULT_USER") ?? "guest");
                    h.Password(Environment.GetEnvironmentVariable("RABBITMQ_DEFAULT_PASS") ?? "guest");
                    h.RequestedConnectionTimeout(TimeSpan.FromSeconds(3));
                });

                cfg.ReceiveEndpoint("buki", q =>
                {
                    // optional - rabbitMQ requires extra packages
                    //var concurrencyLimit = 2;
                    //q.ConcurrentMessageLimit = concurrencyLimit;

                    //q.UseDelayedRedelivery(retry => retry.Incremental(1, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(2)));
                    //q.UseMessageRetry(retry => retry.Incremental(2, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(2)));
                    //q.UsePartitioner(concurrencyLimit, c => c.CorrelationId.ToString());
                    // optional end

                    q.ConfigureConsumer<CommandMessageConsumer>(ctx);
                });
            });
        });
    }
}
