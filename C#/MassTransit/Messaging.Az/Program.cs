using MassTransit;
using Messaging.Az;
using Messaging.Shared;

var builder = WebApplication.CreateBuilder(args);

builder.Services.ConfigureMassTransit();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.MapPost("/send", async (CommandDto dto, IBus bus) =>
{
    var cmd = new CommandMessage
    {
        Id = Guid.NewGuid(),
        Command = dto.Text,
        ThrowException = dto.ThrowException
    };
    
    await bus.Send(cmd);

    return Results.Accepted("Command sent");
})
.WithName("Send Command")
.WithOpenApi();

app.Run();

class CommandDto
{
    public string? Text { get; set; }
    public bool ThrowException { get; set; }
}
