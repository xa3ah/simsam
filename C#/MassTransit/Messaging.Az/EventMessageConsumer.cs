﻿using MassTransit;
using Messaging.Shared;

namespace Messaging.Az;

public class EventMessageConsumer : IConsumer<EventMessage>
{
    public Task Consume(ConsumeContext<EventMessage> context)
    {
        Console.WriteLine($"Consuming event message {context.Message.Id}");

        if (context.Message.ThrowException)
            throw new InvalidOperationException($"Failed to consume {context.Message.Id}");
        else
            Console.WriteLine($"Event log: {context.Message.Text}");
        
        return Task.CompletedTask;
    }
}
