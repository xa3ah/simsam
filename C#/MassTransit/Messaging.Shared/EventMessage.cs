﻿namespace Messaging.Shared;

public class EventMessage
{
    public Guid Id { get; init; }
    public string? Text { get; init; }
    public bool ThrowException { get; init; }
}