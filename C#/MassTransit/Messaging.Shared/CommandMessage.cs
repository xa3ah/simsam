﻿namespace Messaging.Shared;

public class CommandMessage
{
    public Guid Id { get; init; }
    public string? Command { get; init; }
    public bool ThrowException { get; set; }
}
