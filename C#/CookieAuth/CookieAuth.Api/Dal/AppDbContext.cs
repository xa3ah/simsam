﻿using Microsoft.EntityFrameworkCore;

namespace IdentityAudit.Api.Dal;

public class AppDbContext : DbContext
{
    private readonly IdentityData _identityData;

    public AppDbContext(DbContextOptions<AppDbContext> options, IdentityData identityData) : base(options)
    {
        _identityData = identityData;
    }

    public virtual DbSet<DbEntity> DbEntities { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DbEntity>(e =>
        {
            e.ToTable("DbEntity");
            e.HasKey(x => x.Id);
        });
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        var entries = ChangeTracker.Entries();

        foreach (var entry in entries)
        {
            if (entry.State == EntityState.Added && entry.Entity is DbEntity added)
            {
                added.Id = added.Id == Guid.Empty ? Guid.NewGuid() : added.Id;
                added.ModifiedBy = _identityData.User;
            }
            else if (entry.State == EntityState.Modified && entry.Entity is DbEntity modded)
            {
                modded.ModifiedBy = _identityData.User;
            }

        }
        return await base.SaveChangesAsync(cancellationToken);
    }
}

public class DbEntity
{
    public Guid Id { get; set; }
    public string? Text { get; set; }
    public string? ModifiedBy { get; set; }
}