using IdentityAudit.Api;
using IdentityAudit.Api.Dal;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

var builder = WebApplication.CreateBuilder(args);

var dbPath = Path.Combine(builder.Environment.WebRootPath, "db.db3");
var connstr = $"Datasource={dbPath}";

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<CookieStoreEvent>();
builder.Services.AddScoped<IdentityData>();
builder.Services.AddDbContext<AppDbContext>(o => o.UseSqlite(connstr));

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.ExpireTimeSpan = TimeSpan.FromMinutes(20);
        options.SlidingExpiration = true;
        options.AccessDeniedPath = "/Forbidden/";
        options.EventsType = typeof(CookieStoreEvent);
    });
builder.Services.AddAuthorization();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/signin", async (HttpContext ctx) =>
{
    var claims = new List<Claim>
        {
            new Claim(ClaimTypes.Name, "testo"),
            new Claim(ClaimTypes.Role, "Administrator"),
        };

    var claimsIdentity = new ClaimsIdentity(
        claims, CookieAuthenticationDefaults.AuthenticationScheme);

    var authProperties = new AuthenticationProperties();
    await ctx.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
            new ClaimsPrincipal(claimsIdentity),
            authProperties);
});

app.MapGet("signout", async (HttpContext ctx) =>
{
    await ctx.SignOutAsync(
        CookieAuthenticationDefaults.AuthenticationScheme);
});

app.MapGet("data", async ([FromServices] AppDbContext ctx) =>
{
    ctx.DbEntities.Add(new DbEntity
    {
        Text = Guid.NewGuid().ToString()
    });
    await ctx.SaveChangesAsync();
}).RequireAuthorization(b => b.RequireAuthenticatedUser());

app.UseAuthentication();
app.UseAuthorization();




app.Run();


