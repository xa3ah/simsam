﻿using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace IdentityAudit.Api;

public class CookieStoreEvent : CookieAuthenticationEvents
{
    private readonly IdentityData _requestData;

    public CookieStoreEvent(IdentityData requestData)
    {
        _requestData = requestData;
    }

    public override Task ValidatePrincipal(CookieValidatePrincipalContext context)
    {
        var userPrincipal = context.Principal;

        if (userPrincipal != null && userPrincipal.HasClaim(x => x.Type == ClaimTypes.Name))
        {
            var cn = userPrincipal.Claims.First(x => x.Type == ClaimTypes.Name);
            _requestData.User = cn.Value;
            Console.WriteLine(cn.Value);
        }

        return Task.CompletedTask;
    }
}

public class IdentityData
{
    public string? User { get; set; } = string.Empty;
}
