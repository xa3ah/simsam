<Query Kind="Program">
  <Namespace>System.Net.Http</Namespace>
</Query>

async void Main()
{
	var clientId = "some_id";
	var clientSecret = "secret_key";
	var scope = "scope_value";
	
	// mimeType: "application/x-www-form-urlencoded";

	var url = new Uri("http://something");
	MultipartFormDataContent form = new MultipartFormDataContent();
	form.Add(new StringContent("client_credentials"), "grant_type");
	form.Add(new StringContent(scope), "scope");

	var authHeader = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}"));

	var client = new HttpClient();
	client.DefaultRequestHeaders.Add("Authorization", new string[] { $"Basic {authHeader}" });

	var response = await client.PostAsync(url, form);
}
