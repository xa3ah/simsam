<Query Kind="Program" />

void Main()
{
	
}

public interface IVisitor
{
	void Visit(Player player);
}

public interface IVisitable
{
	void Accept(IVisitor visitor);
}

public class Player : IVisitable
{
	private int _health;
	private int _mana;
	
	public void Accept(IVisitor visitor)
	{
		var _ = visitor switch
		{
			ManaPotion mp => _mana += 10,
			HealthPotion mp => _health += 10,
			_ => throw new UnreachableException("Well crap")
		};
	}
	
	// standard implementation
	// public void Accept(IVisitor visitor) => visitor.Visit(this);

	public void AddHealth(int amount) => _health += amount;
	public void AddMana(int amount) => _mana += amount;
}

public class ManaPotion : IVisitor
{
	public void Visit(Player player) => player.AddMana(10);
}

public class HealthPotion : IVisitor
{
	public void Visit(Player player) => player.AddHealth(10);
}
