<Query Kind="Program">
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	
}

void CopyFileInChunks(int chunkSize, string srcFilePath, string destFilePath)
{
    using var reader = new FileStream(srcFilePath, FileMode.Open);
    using var writer = new FileStream(destFilePath, FileMode.OpenOrCreate, FileAccess.Write);

    var readCount = 0;
    var buffer = new byte[chunkSize];

    while ((readCount = reader.Read(buffer, 0, buffer.Length)) != 0)
    {
        writer.Write(buffer, 0, readCount);
    }
}

async Task CopyInChunksSequentiallyAsync(string inputFile, string outputFile, long chunkSize, CancellationToken stoppingToken = default)
{
	using var reader = new FileStream(inputFile, FileMode.Open);
	using var writer = new FileStream(outputFile, FileMode.OpenOrCreate);

	var buffer = new byte[chunkSize];
	var bytesCopied = 0L;
	var remainingBytes = 0L;

	while (bytesCopied < reader.Length)
	{
		remainingBytes = reader.Length - bytesCopied;

		if (buffer.Length > remainingBytes)
		{
			var remainingBuffer = new byte[(int)remainingBytes];
			bytesCopied += await reader.ReadAsync(remainingBuffer, 0, remainingBuffer.Length, stoppingToken);
			await writer.WriteAsync(remainingBuffer, stoppingToken);
		}
		else
		{
			bytesCopied += await reader.ReadAsync(buffer, 0, buffer.Length);
			await writer.WriteAsync(buffer, stoppingToken);
		}
	}
}

async Task CopyInChunksParallelAsync(string inputFile, string outputFile, int chunkSize, int copyTasksCount, CancellationToken stoppingToken = default)
{
	using var reader = new FileStream(inputFile, FileMode.Open, FileAccess.Read, FileShare.Read);
	using var writer = new FileStream(outputFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
	
	writer.SetLength(reader.Length);
	
	if(chunkSize >= reader.Length)
	{
		var buffer = new byte[reader.Length];
		await reader.ReadAsync(buffer, 0, buffer.Length, stoppingToken);
		await writer.WriteAsync(buffer);
		
		return;
	}
	
	var position = 0L;
	var remainingBytes = 0L;
	var chunksCount = 0;
	var copyInfo = new List<(long Position, int BufferSize)>();
	
	while(position < reader.Length)
	{
		remainingBytes = reader.Length - position;

		if (chunkSize > remainingBytes)
		{
			copyInfo.Add((position, (int)remainingBytes));
			position += (int)remainingBytes;
		}
		else
		{
			copyInfo.Add((position, chunkSize));
			position += chunkSize;
		}
		
		chunksCount++;
	}
	
	var utilityCount = chunksCount < copyTasksCount ? chunksCount : copyTasksCount;
	
	var copyTasks = new Task[utilityCount];
	var readers = new FileStream[utilityCount];
	var writers = new FileStream[utilityCount];
	var buffers = new byte[utilityCount][];
	
	for (int i = 0; i < utilityCount; i++)
	{
		readers[i] = new FileStream(inputFile, FileMode.Open, FileAccess.Read, FileShare.Read);
		writers[i] = new FileStream(outputFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
		buffers[i] = new byte[chunkSize];
	}
	
	async Task CopyChunkAsync(long position, int chunkSize, int i)
	{
		readers[i].Position = position;
		writers[i].Position = position;
		
		var buffer = (chunkSize < buffers[i].Length) ? new byte[chunkSize] : buffers[i];
		
		await readers[i].ReadAsync(buffer, 0, chunkSize, stoppingToken);
		await writers[i].WriteAsync(buffer, stoppingToken);
	}
	
	while(copyInfo.Count > 0)
	{
		var tasksCopyInfo = copyInfo.Take(copyTasksCount).ToArray();
		copyInfo.RemoveRange(0, tasksCopyInfo.Length);
		
		for (int i = 0; i < utilityCount; i++)
		{
			copyTasks[i] = CopyChunkAsync(tasksCopyInfo[i].Position, tasksCopyInfo[i].BufferSize, i);
		}
		
		await Task.WhenAll(copyTasks);
	}
	
	for (int i = 0; i < utilityCount; i++)
	{
		readers[i].Dispose();
		writers[i].Dispose();
	}
}
