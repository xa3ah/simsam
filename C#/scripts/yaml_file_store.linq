<Query Kind="Program">
  <NuGetReference>YamlDotNet</NuGetReference>
  <Namespace>YamlDotNet.Serialization</Namespace>
  <Namespace>YamlDotNet.Serialization.NamingConventions</Namespace>
</Query>

void Main()
{
	var ys = YamlFileStore<Category>.Create("");
	var c1 = new Category
	{
		Name = "Melee",
		Children = new List<UserQuery.Category>() {
			new() { Name = "sword" },
			new() { Name = "dagger" },
		}
	};
	ys.Data.Add(c1);
	ys.Complete();
}

public class YamlFileStore<T>
{
	private IList<T> _data;
	private readonly string _filePath;
	private static ISerializer _serializer = new SerializerBuilder()
		.WithNamingConvention(CamelCaseNamingConvention.Instance)
		.Build();
	private static IDeserializer _deserializer =new DeserializerBuilder()
		.WithNamingConvention(CamelCaseNamingConvention.Instance)
		.Build();

	private YamlFileStore(IList<T> data, string filePath)
	{
		_data = data;
		_filePath = filePath;
	}
	
	public static YamlFileStore<T> Create(string filePath)
	{
		if (!File.Exists(filePath))
			File.WriteAllText(filePath, "[]");

		var txt = File.ReadAllText(filePath);
		var data = _deserializer.Deserialize<IList<T>>(txt);

		return new YamlFileStore<T>(data!, filePath);
	}

	public IList<T> Data { get => _data; }

	public void Complete()
	{
		var json = _serializer.Serialize(_data);
		File.WriteAllText(_filePath, json);
	}
}

public class Category
{
	public string Name { get; set; }
	public List<Category> Children { get; set; }
}

public string GetSample() =>
@"
name: Bob
car: &car
  vendor: Lexus
  state: crashed 
mother:
  name: Morenita
  car: *car
father:
  name: Moreno
  car: *car
children:
  - name: Moron
    car: *car
";