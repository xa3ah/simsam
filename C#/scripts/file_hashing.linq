<Query Kind="Program">
  <Namespace>System.Security.Cryptography</Namespace>
</Query>

void Main()
{
	
}

byte[] GetFileContentHash(string filePath, HashAlgorithm hashAlgorithm)
{
	using var file = File.OpenRead(filePath);

	var buffer = new byte[file.Length];
	file.Read(buffer, 0, (int)file.Length);

	return hashAlgorithm.ComputeHash(buffer);
}

byte[] GetFileContentHashInChunksV2(string filePath, HashAlgorithm hashAlgorithm, long chunkSize)
{
	using var file = File.OpenRead(filePath);

	var readCount = 0;
	var buffer = new byte[chunkSize];

	while ((readCount = file.Read(buffer, 0, buffer.Length)) != 0)
	{
		hashAlgorithm.TransformBlock(buffer, 0, readCount, null, 0);
	}

	hashAlgorithm.TransformFinalBlock(buffer, 0, 0);

	return hashAlgorithm.Hash;
}
