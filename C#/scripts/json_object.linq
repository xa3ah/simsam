<Query Kind="Program">
  <Namespace>System.Text.Json.Nodes</Namespace>
</Query>

void Main()
{
	var jObj = new JsonObject()
	{
		["name"] = JsonValue.Create<string>("Nebuchadnezzar"),
		["enemies"] = new JsonArray
		{
			"Zedekiah", "Ithobaal III"		
		}
	};
	
	jObj.ToString();
}
