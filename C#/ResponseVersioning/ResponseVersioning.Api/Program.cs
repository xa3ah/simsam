using Microsoft.AspNetCore.Http.Json;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<JsonOptions>(x =>
{
    x.SerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    x.SerializerOptions.WriteIndented = true;
    x.SerializerOptions.Converters.Add(new DtoConverter());
});

var app = builder.Build();

var dto = new Dto
{
    Id = Guid.NewGuid(),
    Name = "test",
    Numbers = new List<int> { 1, 2 }
};

app.MapGet("v1", () =>
{
    dto.ApiVersion = "v1";
    return Results.Ok(dto);
});

app.MapGet("v2", () =>
{
    dto.ApiVersion = "v2";
    return Results.Ok(dto);
});

app.Run();

class DtoConverter : JsonConverter<Dto>
{
    public override Dto? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return JsonSerializer.Deserialize<Dto>(reader.GetString()!);
    }

    public override void Write(Utf8JsonWriter writer, Dto value, JsonSerializerOptions options)
    {
        writer.WriteStartObject();
        writer.WriteSharedProperties(value, options);

        var _ = value.ApiVersion switch
        {
            "v1" => writer.WriteV1Properties(value, options),
            "v2" => writer.WriteV2Properties(value, options),
            _ => throw new Exception("Unsupported version")
        };

        writer.WriteEndObject();
    }
}

public static class DtoVersioning
{
    public static Unit WriteSharedProperties(this Utf8JsonWriter writer, Dto value, JsonSerializerOptions options)
    {
        var format = GetNameFormatter(options);
        writer.WriteString(format(nameof(Dto.Id)), value.Id);
        return Unit.New;
    }

    public static Unit WriteV1Properties(this Utf8JsonWriter writer, Dto value, JsonSerializerOptions options)
    {
        var format = GetNameFormatter(options);
        writer.WriteString(format(nameof(Dto.Name)), value.Name);
        return Unit.New;
    }

    public static Unit WriteV2Properties(this Utf8JsonWriter writer, Dto value, JsonSerializerOptions options)
    {
        var format = GetNameFormatter(options);
        writer.WriteObject(format(nameof(Dto.Numbers)), value.Numbers, options);
        return Unit.New;
    }

    public static void WriteObject<T>(this Utf8JsonWriter writer, string name, T value, JsonSerializerOptions options)
    {
        var format = GetNameFormatter(options);
        writer.WritePropertyName(format(name));
        JsonSerializer.Serialize<T>(writer, value, options);
    }

    private static Func<string, string> GetNameFormatter(JsonSerializerOptions options)
    {
        if (options.PropertyNamingPolicy != null)
        {
            return options.PropertyNamingPolicy.ConvertName;
        }

        return DefaultNameFormatter;
    }

    private static string DefaultNameFormatter(string x) => x;
}

public class Dto
{
    // shared
    public Guid Id { get; set; }
    // v1 props
    public string? Name { get; set; }
    // v2 props
    public List<int>? Numbers { get; set; }
    // util props
    public string? ApiVersion { get; set; }
}

public struct Unit
{
    public static Unit New => new Unit();
}