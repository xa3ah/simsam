﻿using HelloWorld.Application.Abstractions;

namespace HelloWorld.Application.Services;

public class EmptyFormatter : IMessageFormatter
{
    public string Format(string message) => message;
}
