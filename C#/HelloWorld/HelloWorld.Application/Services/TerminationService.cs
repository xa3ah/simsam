﻿using HelloWorld.Application.Abstractions;

namespace HelloWorld.Application.Services;

public class TerminationService : ITerminationService
{
    private readonly TaskCompletionSource _taskCompletionSource;
    public TerminationService()
    {
        _taskCompletionSource = new();
    }

    public Task GetTask() => _taskCompletionSource.Task;
    public void Terminate() =>_taskCompletionSource.SetResult();
}
