﻿using HelloWorld.Application.Abstractions;

namespace HelloWorld.Application.Services;

public class ConsolePrinter : IMessagePrinter
{
    private readonly IMessageFormatter _formatter;

    public ConsolePrinter(IMessageFormatter formatter)
    {
        _formatter = formatter;
    }
    public void Print(string message)
    {
        var formattedMessage = _formatter.Format(message);
        Console.WriteLine(formattedMessage);
    }
}
