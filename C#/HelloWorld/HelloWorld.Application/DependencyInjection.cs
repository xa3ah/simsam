﻿using HelloWorld.Application.Abstractions;
using HelloWorld.Application.Services;
using Microsoft.Extensions.DependencyInjection;

namespace HelloWorld.Application;

public static class DependencyInjection
{
    public static void AddApplicationServices(IServiceCollection services)
    {
        services
            .AddSingleton<IMessageFormatter, EmptyFormatter>()
            .AddSingleton<IMessagePrinter, ConsolePrinter>()
            .AddSingleton<ITerminationService, TerminationService>();
    }
}
