﻿namespace HelloWorld.Application.Abstractions;

public interface ITerminationService
{
    Task GetTask();
    void Terminate();
}
