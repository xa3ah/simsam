﻿namespace HelloWorld.Application.Abstractions;

public interface IMessageFormatter
{
    string Format(string message);
}
