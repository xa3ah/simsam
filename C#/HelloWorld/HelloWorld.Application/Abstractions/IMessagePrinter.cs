﻿namespace HelloWorld.Application.Abstractions;

public interface IMessagePrinter
{
    void Print(string message);
}
