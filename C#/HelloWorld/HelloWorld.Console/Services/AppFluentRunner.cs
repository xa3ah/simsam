﻿using System;
using HelloWorld.Abstractions.Models;
using HelloWorld.Application.Abstractions;
using HelloWorld.Console.Abstractions;
using HelloWorld.Infrastructure.Handlers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace HelloWorld.Console.Services;

public class AppFluentRunner : IAppConfigBuilder, IAppDependencyInjectionBuilder, IAppRunner
{
    private HostApplicationBuilder _builder;
    private AppFluentRunner()
    {
        _builder = Host.CreateApplicationBuilder();
    }
    public static IAppConfigBuilder Create() => new AppFluentRunner();
    public IAppDependencyInjectionBuilder AddConfiguration()
    {
        _builder.Configuration
            .AddYamlFile("config.yaml", optional: false, reloadOnChange: true);

        _builder.Logging.ClearProviders();
        
        return this;
    }
    public IAppRunner AddDependencyInjection()
    {
        Application.DependencyInjection.AddApplicationServices(_builder.Services);
        Infrastructure.DependencyInjection.AddInfrastructureServices(_builder.Services);
        
        return this;
    }

    public async Task RunAsync()
    {
        var app = _builder.Build();
        var terminator = app.Services.GetRequiredService<ITerminationService>();
        var handler = app.Services.GetRequiredService<SendPrintValue.Handler>();

        await app.StartAsync();
        
        var task = terminator.GetTask();
        
        await handler!.HandleAsync(Unit.New);
        await task;
        await app.StopAsync();
    }
}
