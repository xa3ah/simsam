﻿namespace HelloWorld.Console.Abstractions;

public interface IAppDependencyInjectionBuilder
{
    IAppRunner AddDependencyInjection();
}
