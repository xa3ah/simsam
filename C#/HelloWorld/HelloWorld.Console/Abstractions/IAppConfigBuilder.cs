﻿namespace HelloWorld.Console.Abstractions;

public interface IAppConfigBuilder
{
    IAppDependencyInjectionBuilder AddConfiguration();
}
