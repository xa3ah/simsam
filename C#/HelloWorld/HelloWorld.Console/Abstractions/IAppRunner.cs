﻿namespace HelloWorld.Console.Abstractions;

public interface IAppRunner
{
    Task RunAsync();
}
