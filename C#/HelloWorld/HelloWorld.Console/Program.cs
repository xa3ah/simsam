﻿using HelloWorld.Console.Services;

await AppFluentRunner
    .Create()
    .AddConfiguration()
    .AddDependencyInjection()
    .RunAsync();