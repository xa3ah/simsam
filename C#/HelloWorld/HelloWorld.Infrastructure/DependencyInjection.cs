﻿using HelloWorld.Infrastructure.Abstractions;
using HelloWorld.Infrastructure.Consumers;
using HelloWorld.Infrastructure.Messages;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace HelloWorld.Infrastructure;

public static class DependencyInjection
{
    private const string COMMAND_CONSUMER_ENDPOINT = "console";
    public static void AddInfrastructureServices(IServiceCollection services)
    {
        AddMassTransit(services);
        AddAppHandlers(services);
    }

    private static void AddMassTransit(IServiceCollection services)
    {
        services.AddMassTransit(c =>
        {
            var assembly = typeof(DependencyInjection).Assembly;
            c.AddConsumers(assembly);

            c.UsingInMemory((ctx, cfg) =>
            {
                cfg.ReceiveEndpoint(COMMAND_CONSUMER_ENDPOINT, e =>
                {
                    e.UseDelayedRedelivery(r => r.Exponential(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(3)));
                    e.UseMessageRetry(r => r.Immediate(3));
                    e.ConfigureConsumer<PrintMessageCommandConsumer>(ctx);
                    e.ConfigureConsumer<PrintMessageCommandFaultConsumer>(ctx);
                });

                cfg.Send<PrintMessageCommand>(cmd => cmd.UseCorrelationId(x => x.Id));
            });

            EndpointConvention.Map<PrintMessageCommand>(new Uri($"queue:{COMMAND_CONSUMER_ENDPOINT}"));
        });
    }

    private static void AddAppHandlers(IServiceCollection services)
    {
        var assembly = typeof(DependencyInjection).Assembly;
        var handlerType = typeof(IAppHandler<,>);

        var handlers = assembly
        .GetTypes()
        .Where(x =>
            x.GetInterface(handlerType.Name) != null &&
            !x.IsAbstract &&
            !x.IsInterface)
        .ToList();

        handlers.ForEach(x => services.AddTransient(x));
    }
}
