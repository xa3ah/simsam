﻿namespace HelloWorld.Infrastructure.Abstractions;

public interface IAppHandler<TRequest, TResponse>
{
    Task<TResponse> HandleAsync(TRequest request);
}
