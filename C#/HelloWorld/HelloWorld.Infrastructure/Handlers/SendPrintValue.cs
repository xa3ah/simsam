﻿using HelloWorld.Abstractions.Models;
using HelloWorld.Infrastructure.Abstractions;
using HelloWorld.Infrastructure.Messages;
using MassTransit;
using Microsoft.Extensions.Configuration;

namespace HelloWorld.Infrastructure.Handlers;

public static class SendPrintValue
{
    public class Handler : IAppHandler<Unit, Unit>
    {
        private readonly IBus _bus;
        private readonly IConfiguration _configuration;

        public Handler(IBus bus, IConfiguration configuration)
        {
            _bus = bus;
            _configuration = configuration;
        }

        public async Task<Unit> HandleAsync(Unit request)
        {
            var printingValue = _configuration
                .GetSection("printing")
                .GetValue<string>("value");

            if(printingValue == null )
                throw new InvalidOperationException("Printing value not found in config file");

            await _bus.Send<PrintMessageCommand>(new(Guid.NewGuid(), printingValue));

            return Unit.New;
        }
    }
}
