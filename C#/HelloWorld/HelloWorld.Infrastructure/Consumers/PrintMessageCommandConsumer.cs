﻿using HelloWorld.Application.Abstractions;
using HelloWorld.Infrastructure.Messages;
using MassTransit;

namespace HelloWorld.Infrastructure.Consumers;

public class PrintMessageCommandConsumer : IConsumer<PrintMessageCommand>
{
    private readonly IMessagePrinter _printer;
    private readonly IMessageFormatter _messageFormatter;
    private readonly ITerminationService _terminationService;

    public PrintMessageCommandConsumer(
        IMessagePrinter printer,
        IMessageFormatter messageFormatter,
        ITerminationService terminationService)
    {
        _printer = printer;
        _messageFormatter = messageFormatter;
        _terminationService = terminationService;
    }

    public Task Consume(ConsumeContext<PrintMessageCommand> context)
    {
        if (context.Message == null)
            throw new InvalidOperationException("Received message is null");

        var formattedMessage = _messageFormatter.Format(context.Message.Text);
        _printer.Print(formattedMessage);
        _terminationService.Terminate();

        return Task.CompletedTask;
    }
}
