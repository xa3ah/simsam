﻿using HelloWorld.Application.Abstractions;
using HelloWorld.Infrastructure.Messages;
using MassTransit;

namespace HelloWorld.Infrastructure.Consumers;

public class PrintMessageCommandFaultConsumer : IConsumer<Fault<PrintMessageCommand>>
{
    private readonly ITerminationService _terminationService;

    public PrintMessageCommandFaultConsumer(ITerminationService terminationService)
    {
        _terminationService = terminationService;
    }

    public Task Consume(ConsumeContext<Fault<PrintMessageCommand>> context)
    {
        Console.WriteLine("Message consuming faulted");
        _terminationService.Terminate();
        return Task.CompletedTask;
    }
}
