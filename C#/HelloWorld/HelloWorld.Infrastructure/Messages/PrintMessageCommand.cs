﻿namespace HelloWorld.Infrastructure.Messages;

public record PrintMessageCommand(Guid Id, string Text);
