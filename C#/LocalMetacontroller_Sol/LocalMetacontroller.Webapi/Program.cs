using k8s.Models;
using LocalMetacontroller.Webapi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

var jsonOptions = new JsonSerializerOptions
{
    WriteIndented = true,
};

void Log(object obj) => Console.WriteLine(JsonSerializer.Serialize(obj, jsonOptions));

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.MapPost("/pod/sync", ([FromBody] HookRequest<V1Pod> request) =>
{
    Log("Sync");
    Log(request);
    
    return Results.Ok(new HookResponse());
});

app.MapPost("/pod/finalize", ([FromBody] HookRequest<V1Pod> request) =>
{
    Log("Finalize");
    Log(request);
    
    return Results.Ok(new HookResponse() { Finalized = true });
});

app.MapPost("/job/sync", ([FromBody] HookRequest<V1Job> request) =>
{
    Log("Sync");
    Log(request);

    return Results.Ok(new HookResponse());
});

app.MapPost("/job/finalize", ([FromBody] HookRequest<V1Job> request) =>
{
    Log("Finalize");
    Log(request);

    return Results.Ok(new HookResponse() { Finalized = true });
});

app.Run();


