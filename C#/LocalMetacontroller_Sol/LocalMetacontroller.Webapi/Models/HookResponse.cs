﻿using System.Text.Json.Nodes;

namespace LocalMetacontroller.Webapi.Models;

public class HookResponse
{
    public Dictionary<string, string>? Labels { get; set; }
    public JsonObject? Annotations { get; set; }
    public JsonObject? Status { get; set; }
    public JsonObject[]? Attachments { get; set; }
    public float? ResyncAfterSeconds { get; set; }
    public bool Finalized { get; set; }
}
