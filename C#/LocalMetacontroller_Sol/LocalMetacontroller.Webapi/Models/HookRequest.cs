﻿namespace LocalMetacontroller.Webapi.Models;

public class HookRequest<T> where T : class
{
    public bool Finalizing { get; set; }
    public T? Object { get; set; }
}
