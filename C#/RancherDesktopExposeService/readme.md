# Exposing stuff in Rancher Desktop

- Open terminal in this dir and build docker image with: `docker build -f Sample.Api\Dockerfile -t sample_api .`

- Deploy deployment with command: `kubectl apply -f Sample.Api\deployment.yaml`

- Forward service port to 50000 using Rancher desktop, and go to `http://localhost:50000` 