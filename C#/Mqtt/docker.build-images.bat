docker build -f ./Messager.MqttBroker/Dockerfile -t messager_broker .
docker build -f ./Messager.IotDevice/Dockerfile -t messager_iot .
docker build -f ./Messager.WebApi/Dockerfile -t messager_api .