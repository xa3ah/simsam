import type { Seat } from "./seat";

export type SeatMap = {
    row: number;
    seats: Seat[];
}