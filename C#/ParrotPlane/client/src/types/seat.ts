export type Seat = {
    row: number;
    mark: string;
    reserved: boolean;
}