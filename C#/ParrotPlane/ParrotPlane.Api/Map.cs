﻿using System.Diagnostics;

namespace ParrotPlane.Api;

public static class Map
{
    public static IResult ToResult(object? obj = null) =>
        obj switch
        {
            null => Results.NotFound(),
            _ => Results.Ok(obj)
        };

    public static IResult ToResult(eOpcode opcode, string? obj = null) =>
        opcode switch
        {
            eOpcode.Ok => Results.Ok(obj),
            eOpcode.NotFound => Results.NotFound(),
            eOpcode.Rejected => Results.Conflict(),
            _ => throw new UnreachableException("oops")
        };
}
