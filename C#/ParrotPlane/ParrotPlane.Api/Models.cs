﻿namespace ParrotPlane.Api;

public class Flight
{
    public int Id { get; set; }
    public string? From { get; set; }
    public string? To { get; set; }
    public Seat[] Seats { get; set; } = Array.Empty<Seat>();
}

public class Seat
{
    public int Row { get; set; }
    public char Mark { get; set; }
    public bool Reserved { get; set; }
}

public class Reservation
{
    public int FlightId { get; set; }
    public Seat[] Seats { get; set; } = Array.Empty<Seat>();
    public string[] Passports { get; set; } = Array.Empty<string>();
}
