using Microsoft.AspNetCore.Mvc;
using ParrotPlane.Api;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateSlimBuilder(args);

builder.Services.ConfigureHttpJsonOptions(options =>
{
    options.SerializerOptions.TypeInfoResolverChain.Insert(0, AppJsonSerializerContext.Default);
});

builder.Services.AddCors(o => o.AddDefaultPolicy(b => b.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin()));

builder.Services.AddSingleton(Database.Create(builder.Environment));

var app = builder.Build();

app.UseStaticFiles();

app.UseCors();

var flights = app.MapGroup("/flights");

app.MapGet("", (IWebHostEnvironment env) =>
{
    var file = Path.Combine(env.WebRootPath, "index.html");
    return Results.File(file, "text/html");
});

flights.MapGet("", ([FromServices] Database db) =>
{
    return Map.ToResult(db.GetAll());
});

flights.MapGet("find", ([FromQuery] int flightId, [FromServices] Database db) =>
{
    return Map.ToResult(db.Find(flightId));
});

flights.MapPost("reserve", ([FromBody] Reservation reservation, [FromServices] Database db) =>
{
    var (opcode, message) = db.Reserve(reservation);
    return Map.ToResult(opcode, message);
});

app.Run();

[JsonSerializable(typeof(Flight))]
[JsonSerializable(typeof(Flight[]))]
[JsonSerializable(typeof(Seat))]
[JsonSerializable(typeof(Seat[]))]
[JsonSerializable(typeof(Reservation))]
internal partial class AppJsonSerializerContext : JsonSerializerContext {}