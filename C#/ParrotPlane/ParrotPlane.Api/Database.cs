﻿using System.Text.Json;

namespace ParrotPlane.Api;

public class Database
{
    private readonly string _jsonPath;
    private readonly Flight[] _flights;
    protected Database(string jsonPath, Flight[] flights)
    {
        _jsonPath = jsonPath;
        _flights = flights;
    }

    public static Database Create(IWebHostEnvironment env)
    {
        var jsonPath = Path.Combine(env.WebRootPath, "data.json");
        var flights = Read(jsonPath);
        
        flights = flights.Length == 0 ? SeedFlights() : flights;

        return new Database(jsonPath, flights);
    }

    public Flight[] GetAll() => _flights;
    public Flight? Find(int id) => Array.Find(_flights, f => f.Id == id);
    public (eOpcode, string) Reserve(Reservation reservation)
    {
        var flight = Array.Find(_flights, f => f.Id == reservation.FlightId);
        
        if (flight == null)
            return (eOpcode.NotFound, "flight not found");

        var requestedSeats = new List<Seat>();
        foreach(var fs in flight.Seats)
        {
            var rs = reservation.Seats.FirstOrDefault(x =>
                x.Row == fs.Row &&
                x.Mark == fs.Mark
            );

            if (rs != null)
                requestedSeats.Add(fs);
        }
        
        var allSeatFree = Array.TrueForAll(requestedSeats.ToArray(), x => !x.Reserved);

        if(!allSeatFree)
            return (eOpcode.Rejected, "not all seats are free");

        requestedSeats.ForEach(x => x.Reserved = true);

        Save();

        return (eOpcode.Ok, string.Empty);
    }
    private void Save()
    {
        var json = JsonSerializer.Serialize(_flights, AppJsonSerializerContext.Default.FlightArray)!;
        File.WriteAllText(_jsonPath, json);
    }

    private static Flight[] Read(string jsonDataPath)
    {
        if (!File.Exists(jsonDataPath))
            throw new ArgumentException("Data json not found");

        var json = File.ReadAllText(jsonDataPath);
        
        return JsonSerializer.Deserialize(json, AppJsonSerializerContext.Default.FlightArray)!;
    }
    private static Flight[] SeedFlights()
    {
        var rc = 2;
        return [
            new() { Id = 1, From = "London", To = "Paris", Seats = GenerateSeats(rc, 6) },
            new() { Id = 2, From = "Dubai", To = "Kuala Lumpur", Seats = GenerateSeats(rc, 6) },
            new() { Id = 3, From = "Shanghai", To = "Havana", Seats = GenerateSeats(rc, 6) },
            new() { Id = 4, From = "Sao Paolo", To = "Tbilisi", Seats = GenerateSeats(rc, 6) },
            new() { Id = 5, From = "Barcelona", To = "Vladivostok", Seats = GenerateSeats(rc, 6) }
        ];
    }

    private static Seat[] GenerateSeats(int rowCount, int rowLength)
    {
        var rand = new Random();
        var list = new List<Seat>();
        for (int i = 1; i <= rowCount; i++)
        {
            for (int j = 0; j < rowLength; j++)
            {
                var rowChar = (char)(j + 97);
                list.Add(new Seat
                {
                    Mark = rowChar,
                    Row = i,
                    Reserved = rand.Next(0, 100) % 2 == 0
                });
            }
        }

        return list.ToArray();
    }
}
