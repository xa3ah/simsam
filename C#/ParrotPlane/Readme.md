# Native AOT server with AstroJS generated static site (with svelte)

## Build
Navigate to `client` directory and run:
```
npm i
npm run build
```
copy the **contents** of `client/dist` directory to `ParrotPlane.Api/wwwroot` directory.