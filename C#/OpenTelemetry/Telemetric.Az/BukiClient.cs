﻿using System.Text;
using System.Text.Json;
using Telemetric.Shared.Models;

namespace Telemetric.Az;

public class BukiClient
{
    private readonly HttpClient _httpClient;
    public BukiClient(IHttpClientFactory factory)
    {
        _httpClient = factory.CreateClient();
    }

    public async Task<ProductRequest[]> GetOrdersAsync()
    {
        var response = await SendAsync("orders", HttpMethod.Get);
        var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        
        return JsonSerializer.Deserialize<ProductRequest[]>(content)!;
    }

    public Task PostOrderAsync(ProductRequest request) => SendAsync("orders", HttpMethod.Post, request);

    private async Task<HttpResponseMessage> SendAsync(string endpoint, HttpMethod method, object? body = null)
    {
        var bukiAddress = Environment.GetEnvironmentVariable("BUKI_ADDRESS");
        if (bukiAddress == null)
            throw new InvalidOperationException("Buki address not set in environment");

        var message = new HttpRequestMessage();

        message.Method = method;
        message.RequestUri = new Uri($"{bukiAddress}/{endpoint}");

        if(body != null)
        {
            message.Content = new StringContent(JsonSerializer.Serialize(body), Encoding.UTF8, "application/json");
        }

        var response = await _httpClient.SendAsync(message);
        response.EnsureSuccessStatusCode();

        return response;
    }
}
