import { glob } from 'astro/loaders';
import { z, defineCollection, reference } from 'astro:content';

export const collections = {
    posts: defineCollection({
        loader: glob({pattern: '**.*{md,mdx}', base: 'src/content/posts'}),
        schema: z.object({
            title: z.string(),
            author: reference('authors'),
            date: z.date(),
            tags: z.array(z.string())
        })
    }),
    authors: defineCollection({
        loader: glob({pattern: '**.*{yml,yaml}', base: 'src/content/authors'}),
        schema: z.object({
            name: z.string(),
            fields: z.array(z.string())
        })
    })
}