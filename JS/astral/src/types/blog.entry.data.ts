export interface BlogEntryData {
    title: string;
    tags: string[];
    urlPath: string;
}