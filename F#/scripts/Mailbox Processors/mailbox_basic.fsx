let agent = MailboxProcessor.Start(fun mailbox ->
    let rec loop () =
        async {
            let! message = mailbox.Receive()
            printfn "Received message %s" message
            do! loop ()
        }

    loop ()    
)

let postMessage n =
    async {
        let message = sprintf "Message %i" n
        agent.Post(message)
    }

[1..10]
|> List.map (fun x -> postMessage x)
|> Async.Parallel
|> Async.RunSynchronously