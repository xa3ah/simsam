type Priority = Important | Regular

type Message = {
    Priority: Priority
    Text: string
}

let agent = MailboxProcessor.Start(fun mailbox ->
        async {
            do! mailbox.Scan(fun message ->
                match message.Priority with
                | Important ->
                    Some (async {
                        printfn "Warning: %s" message.Text
                    })
                | Regular -> None
            )
        }
    )