type Message = string * AsyncReplyChannel<string>

let agent = MailboxProcessor<Message>.Start(fun mailbox ->
    let rec loop () = async {
        let! (message, replyChannel) = mailbox.Receive()
        let response = sprintf "Got message: %s" message
        replyChannel.Reply(response)
        do! loop ()
    }

    loop ()
)

agent.PostAndReply(fun replyChannel -> "Working", replyChannel )