type Reservation = {
    Table: int
    Name: string
}

type IRepository =
    abstract member GetTables: unit -> Async<list<int>>
    abstract member GetReservations: unit -> Async<list<Reservation>>
    abstract member AddReservation: Reservation -> Async<Reservation>

type Repository() =
    interface IRepository with
        member this.GetTables() = async {
            return [1..4]
        }

        member this.GetReservations() = async {
            return [
                { Name = "Alladin"; Table = 1 }
                { Name = "Jasmine"; Table = 3 }
            ]
        }

        member this.AddReservation (reservation: Reservation) = async {
            return reservation
        }
        
// top level (IO)
let addReservationFlow
    (repo: IRepository)
    (reservation: Reservation)
    (validator: Reservation -> Result<Reservation, string>)
    (tryAcceptReservation: int list -> Reservation -> Reservation list -> Result<Reservation, string>)
    : Async<Result<Reservation, string>>
    = async {
        let validated = validator reservation
        match validated with
        | Error e -> return Error e
        | Ok reservation ->
            let! tables = repo.GetTables()
            let! reservations = repo.GetReservations()
            let result = tryAcceptReservation tables reservation reservations

            match result with
            | Ok r ->
                let! reservation = repo.AddReservation(r)
                return Ok reservation
            | Error e -> return Error e
    }

// Reservation module
let tryAcceptReservation (tables: int list) (reservation: Reservation) (reservations: Reservation list) =
    let busyTables = reservations |> List.map (fun x -> x.Table)
    let freeTables = 
        tables
        |> List.filter (fun x -> not (busyTables |> (List.contains x)))

    match freeTables with
    | [] -> Error "No free tables left"
    | tables ->
        if (tables |> List.contains reservation.Table)
        then Error "Table already reserved"
        else Ok reservation