open System
open System.IO

type Process = (unit -> unit)

type InstructionType =
| Action of Process
| Loop of (Process * int)
| Parallel of Process list

type Instruction = {
    Id: Guid
    Type: InstructionType
    Next: Instruction option
}

let actionHandler (action: Process) = action ()
let loopHandler (p: Process) (i: int) = [1..i] |> List.iter (fun _ -> p ())
let parallelHandler (ps: Process list) = 
    let executeAsync (p: Process) = async { do p () }
    ps
    |> List.map (fun p -> executeAsync p)
    |> Async.Parallel
    |> Async.RunSynchronously
    |> ignore

type InstructionBuilder() = 
    let mutable _instruction = {
        Id = Guid.Empty
        Type = InstructionType.Action (fun () -> ())
        Next = None
    }


    member this.Print() = printfn "%A" _instruction
    

    member this.AddNext(next: Instruction, ?parentId: Guid) =
        let pid = parentId |> Option.defaultValue Guid.Empty

        let rec addNext current next =
            if current.Id = pid then
                Ok ({ current with Next = Some next})
            
            elif current.Next.IsSome then
                let newNext = addNext current.Next.Value next
                match newNext with
                | Ok next -> Ok ({current with Next = Some next })
                | Error e -> Error e
            
            else Error "No parent process found"
        
        let result = addNext _instruction next
        
        match result with
        | Ok ins -> _instruction <- ins; Ok ()
        | Error e -> Error e


    member this.Execute() =
        let rec exec x =
            match x with
            | None -> ()
            | Some x ->
                match x.Type with
                | Action a -> actionHandler a
                | Loop (f, i) -> loopHandler f i
                | Parallel ps -> parallelHandler ps
                exec x.Next

        exec (Some _instruction)

let builder = InstructionBuilder()


let mapToInstruction (itype: InstructionType) =
    { Id = Guid.NewGuid(); Type = itype; Next = None }


let createOutputDir () =
    let path = Path.Combine(__SOURCE_DIRECTORY__, "Outputs")
    Directory.CreateDirectory(path) |> ignore
    printfn "EXECUTING CREATE OUTPUT DIR"


let createOutputFile () =
    let outputDir = Path.Combine(__SOURCE_DIRECTORY__, "Outputs")
    let files = Directory.GetFiles(outputDir)

    let fileNumber = files |> Array.length
    let filename = sprintf "testfile_%i.txt" fileNumber
    let path = Path.Combine(outputDir, filename)
    File.Create(path) |> ignore
    printfn "EXECUTING CREATE OUTPUT FILE"


let getFilePath fileNumber =
    let outputDir = Path.Combine(__SOURCE_DIRECTORY__, "Outputs")
    let filename = sprintf "testfile_%i.txt" fileNumber
    printfn "EXECUTING POPULATE OUTPUT FILE %i" fileNumber
    Path.Combine(outputDir, filename)

let populateFile1 () =
    let path = getFilePath 1
    File.WriteAllText(path, "This was populated b method 1")

let populateFile2 () =
    let path = getFilePath 2
    File.WriteAllText(path, "This was populated b method 2")

let populateFile3 () =
    let path = getFilePath 3
    File.WriteAllText(path, "This was populated b method 3")


let makeOutputDirIns = mapToInstruction (Action createOutputDir)
let makeFilesIns = mapToInstruction (Loop (createOutputFile, 3))
let populateFilesIns = mapToInstruction (Parallel ([populateFile1; populateFile2; populateFile3]))


builder.AddNext(makeOutputDirIns, Guid.Empty)
builder.AddNext(makeFilesIns, makeOutputDirIns.Id)
builder.AddNext(populateFilesIns, makeFilesIns.Id)

builder.Print()
builder.Execute()