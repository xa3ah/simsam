type Person = {
    Name: string
    Child: Person option
}

type Personizer() =
    let mutable root = { Name = ""; Child = None }

    member this.AddChild(child: Person, ?parent: string) =
        let p = parent |> Option.defaultValue ""

        let rec addChild parent child =
            if parent.Name = p then
                Ok ({ parent with Child = Some child})
            
            elif parent.Child.IsSome then
                let newChild = addChild parent.Child.Value child
                match newChild with
                | Ok nc -> Ok ({ parent with Child = Some nc })
                | Error e -> Error e
            
            else Error "No parent found"
        
        let result = addChild root child
        
        match result with
        | Ok r -> root <- r; Ok ()
        | Error e -> Error e
        

    member this.Print() = printfn "%A" root


let alice = { Name = "alice"; Child = None }
let bob = { Name = "bob"; Child = None }
let carl = { Name = "carl"; Child = None }

let pszer = Personizer()

pszer.AddChild(alice)
pszer.AddChild(bob, "alice")
pszer.AddChild(carl, "bob")
pszer.Print()