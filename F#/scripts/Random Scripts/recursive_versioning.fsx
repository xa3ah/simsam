type Version<'a> = {
    Object: 'a
    NewVersion: Version<'a> option
}

module Version =
    let getLatest (x: Version<'a>) =
        let rec dig (x: Version<'a>) =
            match x.NewVersion with
            | None -> x
            | Some newv -> dig newv
        
        dig x

    let addNew (src: Version<'a>) (transform: 'a -> 'a) =
        let rec add x =
            match x.NewVersion with
            | Some newVersion ->
                let ver = add newVersion
                { x with NewVersion = Some ver }
            | None ->
                let obj = x.Object |> transform
                let newVersion = Some { Object = obj; NewVersion = None }
                { x with NewVersion = newVersion }

        add src

let add1 x = x + 1

let v1 = { Object = 0; NewVersion = None }

let v4 = 
    [ add1; add1; add1 ]
    |> List.fold (fun acc transform -> Version.addNew acc transform) v1

printfn "%A" v4