// create event
let event1 = new Event<string>()

// create event handler for event
let eventHandler1 = event1.Publish

// add callback
eventHandler1.Add(fun x -> printfn "Event triggerd with mesage %s" x)

// trigger event
event1.Trigger("I am event one")