﻿open System.Net.Http
open System.Text

type LoginDto = {
    Name: string
}

type TestRequest = {
    Dto: LoginDto
    Client: HttpClient
}


let url = "https://localhost:7185/user/login"

let sendRequest treq = async {
    let json = System.Text.Json.JsonSerializer.Serialize(treq.Dto)
    let content = new StringContent(json, Encoding.UTF8, "application/json")
    let! response = treq.Client.PostAsync(url, content) |> Async.AwaitTask
    let! responseString = response.Content.ReadAsStringAsync() |> Async.AwaitTask

    return responseString
}

let results =
    [
        { Dto = { Name = "alice" }; Client = new HttpClient() }
        { Dto = { Name = "bob" }; Client = new HttpClient() }
    ]
    |> List.map (fun x -> sendRequest x)
    |> Async.Parallel
    |> Async.RunSynchronously

results |> Array.iter (fun x -> printfn "%s" x)

