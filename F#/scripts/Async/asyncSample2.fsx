open System.Net.Http

let fetchPageAsync (url: string) =
    async {
        let httpClient = new HttpClient()
        let! result = httpClient.GetStringAsync(url) |> Async.AwaitTask
        
        return result
    }


let longWaitAsync (time: int) =
    async {
        Async.Sleep time |> ignore
        printfn "Im done sleeping"

        return ()
    }


let wiki = "https://www.wikipedia.org"
let page = fetchPageAsync wiki |> Async.RunSynchronously
printfn "%s" page
