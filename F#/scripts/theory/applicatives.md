# Applicatives

Since this is the key to clarify your confusion I will briefly expand on it.
`Seq` accepts two applicative set of functions, let's call them "cardinal product" and "pairwise".

So, result for cardinal product is Seq.singleton whereas apply is a functions which applies a function for each function in the `[f; g; ..] seq` to each argument in `[x; y ..]` in the arguments seq, giving you the result of `[f x; f y ..., g x; g y; ...]`

The other possible applicative is the one with pairwise application, here result would be an infinite sequence of the original function, and <*> will be something that zips both lists (up to the shortest one) and applies functions of the first seq to arguments of the second one pairwise, that is something like `[f x; g y]`.

None of these 2 applicatives breaks the rule I explained:
`result f <*>`  (with any of both applicatives) is still equivalent to `map f x` .
I won't go into monads territory, but I will just mention that the first applicative is also a monad, but the second one (the pairwise implementation) is not.

Remember this: all monads are applicative functors, all applicative functors are functors.  The opposite is not true.

Now this situation is more common than what it seems:

`Async` has also 2 possible applicatives, one is also a monad.

`Result` has 2 possible applicatives as well, one being also a monad but not the second one.

let's peek on this last one:

One possible applicative for Result is short-circuit validation, which is also a monad (bind can be implemented and it makes sense).

The other possible applicative for Result is accumulative validation, which doesn't form a monad (bind can not be implemented in such a way that would obey monad laws).

And here's the mother of all confusions: some library and/or blog authors call the first one the monadic validation and the second one the applicative validation, which is PLAIN WRONG and confuses every developer who's trying to understand FP.

Why is wrong? because as you can figure it out with all what I explained so far, BOTH validations are APPLICATIVE. the fact that the first one can also be a monad doesn't prevent it to be an applicative validation as well.

Here's the second confusion, which is more of the same stuff.
It's not just naming, but when you name things wrong you tend to write wrong stuff as well:

Some libraries present Computation Expressions which when using monadic operations uses one behavior, but when using applicative operations switches to the other one.

For validations what happens is that monadic operations uses the short-circuit applicative-monad, but for applicative operations, instead of being consistent and keep using the short circuit one, they use the accumulative one, making it impossible to reason about it and preventing generic optimizations which potentially would use applicative/monadic properties, to work in a consistent way, plus some other inconsistent situations that don't come to my mind just now.

So, the right thing to do is:
- First and foremost: name things as what they are: short-circuit validation is short circuit validation, accumulative validation is accumulative validation. Avoid naming them as applicative or monadic validation as it's unnecessarily confusing since not everybody understand what applicative and monad is, and last but not least because those names are plain wrong.
- Provide separate computation expressions for each type of validation, don't mix them in the same CE, it will bite you sooner or later.

For Validation, the only CE in which you can implement Bind and BindReturn is in short-circuit validation