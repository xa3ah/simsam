# Functor laws

## 1. They must define a structure preserving map function

`[1..10] |> List.map (fun x -> x.ToString())`

## 2. They must have an identity function

A function which takes the original functor as parameter and returns it unchanged

`let id f = f`

## 3. Using map & id function combined, returns the original functor

`f |> List.map id = f`

## 4. Order of operation doesn't matter

If you have 2 functions (`g, h`), mapping over one after the other should be the same as mapping over the composition of `g` and `h`.

```
let g x = x.ToString()
let h x = int(x)

let r1 = f |> List.map g |> List.map h
let r2 = f |> List.map (fun x -> h(g(x)))

r1 = r2
```

### Testing it all with an F# list

```
let firstLaw f =
    f |> List.map (fun x -> x.ToString())

let secondLaw f =
    id f = f

let thirdLaw f =
    f |> List.map id = f

let fourthLaw f =
    let g x = x.ToString()
    let h x = int(x)
    let r1 = f |> List.map g |> List.map h
    let r2 = f |> List.map (fun x -> h(g(x)))
    r1 = r2

let laws f =
    firstLaw    f &&
    secondLaw   f &&
    thirdLaw    f &&
    fourthLaw   f

match [] with
| f when laws f -> "functor"
| _ -> "functor nyet"
```