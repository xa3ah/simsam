open System
open System.Net.Http
// https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/flexible-types

// automatic upcasting
#FlexibleType
let dispose (x: IDisposable) = x.Dispose()
let http = new HttpClient()
dispose http


let https = seq {1..5} |> Seq.map (fun _ -> new HttpClient())
let disposeMany(xs: seq<IDisposable>) =
    xs |> Seq.iter (fun x -> x.Dispose())

let disposeMany2(xs: seq<#IDisposable>) =
    xs |> Seq.iter (fun x -> x.Dispose())

disposeMany https
disposeMany2 https