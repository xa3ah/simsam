let l1 = lazy 6

let l2 = lazy (
    seq {
        for i in [0..10] do
            yield i
    }
)

l1.Value
l2.Value