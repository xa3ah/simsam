// DU named params
type Vector = | TwoD of x: int * y: int

let vector2D = TwoD (x = 2, y = 3)


// Nested patterns
type Contact = | Email of email: string | Tel of number: string

type User = {
  PrimaryContact: Contact option
  OtherContacts: Contact list
}

let tryGetAnyContact user =
    match user with
    | { PrimaryContact = Some(contact) } -> Some contact
    | { OtherContacts = telNumbers } when telNumbers <> [] -> Some telNumbers[0]
    | _ -> None


// One line pattern match
let oneLinePatternMatch = function | Some _ -> () | None -> ()

// nested matching
type Foo = | A of string | B of int | C

let rec bar (foos : Foo list) =
    match foos with
    | A str :: tail ->
        printfn $"A {str}"
        bar tail
    | B i :: tail ->
        printfn $"B {i}"
        bar tail
    | C :: tail ->
        printfn "C"
        bar tail
    | [] ->
        ()

// union types with functions
type Foo =
    | Numbers of int list
    | Strings of string list
    
    member this.Do () =
        match this with
        | Numbers xs -> xs |> List.map (fun x -> sprintf "%i" (x * x))
        | Strings xs -> xs |> List.map (fun x -> x.ToUpper())

(Numbers [1;2;3]).Do () |> printfn "%A"
(Strings ["shout"; "cloud"]).Do () |> printfn "%A"

let foo<'TReq, 'TResp when 'TReq :> Bar and 'TResp :> IBaz> 
        (handler: IHandler<'TReq, 'TResp>) = 
        failwith "Not implemented"
