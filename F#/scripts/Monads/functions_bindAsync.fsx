let bindAsync f xr = async {
    match xr with
    | Ok x ->
        let! result = f x
        return Ok result
    | Error e -> return Error e
}

let validate x = if x < 2 then Error "" else Ok x
let getList x = async { return [1..x] }

let r =
    validate 0
    |> bindAsync getList