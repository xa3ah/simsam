let bestSum (target: int) (numbers: int list) =
    let mutable memo: Map<int, int list option> = Map[]
    
    let rec innerBest tgt =
        let mutable shortest: int list option = None
        let (isComputed, combination) = memo.TryGetValue(tgt)
        
        if isComputed then combination else
        match tgt with
        | 0 -> Some []
        | x when x < 0 -> None
        | _ ->
            numbers |> List.iter (fun num ->
                let remainder = tgt - num
                let combination = innerBest remainder
                match combination with
                | None -> ()
                | Some comb ->
                    let newComb = (num::comb)
                    match shortest with
                    | None -> shortest <- Some newComb
                    | Some sh ->
                        let newShortest = if (newComb.Length < sh.Length) then newComb else sh
                        shortest <- Some newShortest
            )
            
            memo <- memo.Add(tgt, shortest)
            shortest
            
            

    innerBest target

let best = bestSum 128 [3; 4; 12; 6 ;7]
printfn "%A" best