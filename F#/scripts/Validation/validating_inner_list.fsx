type ResultExpression() =
    member this.Bind(x, f) = Result.bind f x
    member this.Return(x) = Ok x

// utility module for validating sequences
module SequenceValidator =
    
    let private accumulateWithErrorList xResult state =
        match xResult, state with
        | Ok x, Ok s -> Ok (x::s)
        | Ok _, Error s -> Error s
        | Error x, Ok _ -> Error [x]
        | Error x, Error s -> Error (x::s)


    let private accumulateWithError xResult state errorMsg =
        match xResult, state with
        | Ok x, Ok s -> Ok (x::s)
        | _ -> Error errorMsg


    let validateWithErrorList xs validator =
        let folder acc x = accumulateWithErrorList (validator x) acc
        xs |> List.fold folder (Ok List.Empty)


    let validateWithError xs validator errorMsg =
        let folder acc x = accumulateWithError (validator x) acc errorMsg
        xs |> List.fold folder (Ok List.Empty)


// simple type
type String5 = String5 of string
module String5 =
    let from (str: string) =
        match isNull str || str.Length < 5 with
        | true -> Error "Bad input"
        | false -> Ok (String5 str)

    let unwrap (String5 str) = str

// models
type Chapter = { Name: String5; PageCount: int }
type Book = { Title: String5; Chapters: Chapter list }

// dtos
type ChapterDto = { Name: string; PageCount: int }
type BookDto = { Title: string; Chapters: ChapterDto list }

// test data

let goodBookDto = { Title = "Gastronomy"; Chapters = [{ Name = "Veggies"; PageCount = 50 }] }
let badBookDto = {
    Title = "Astronomy"
    Chapters = [
        { Name = "Mercury"; PageCount = 50 }
        { Name = "Venus"; PageCount = 50 }
        { Name = "Mars"; PageCount = 50 }
        { Name = "Moon"; PageCount = 50 }
    ]
}

// tadaa 

let bookFromDto_SingleErrorForChapters(bookDto: BookDto) =
    let builder = ResultExpression()
    let sequenceValidator = SequenceValidator.validateWithError
    let chapterValidator (dto: ChapterDto) =
        builder {
            let! name = String5.from dto.Name
            let chapter: Chapter = { Name = name; PageCount = dto.PageCount}
            return chapter
        }

    let result = builder {
        let! title = String5.from bookDto.Title
        let! chapters = sequenceValidator bookDto.Chapters chapterValidator "One of the chapters failed to convert"
        let book: Book = { Title = title; Chapters = chapters }

        return book
    }

    result


let bookFromDto_AllErrors(bookDto: BookDto) =
    let builder = ResultExpression()
    let sequenceValidator = SequenceValidator.validateWithErrorList
    let chapterValidator (dto: ChapterDto) =
        builder {
            let! name = String5.from dto.Name
            let chapter: Chapter = { Name = name; PageCount = dto.PageCount}
            return chapter
        }

    let title = String5.from bookDto.Title
    let chapters = sequenceValidator (bookDto.Chapters) (chapterValidator)

    match chapters, title with
    | Ok chapters, Ok title -> Ok ({ Title = title; Chapters = chapters } : Book)
    | Ok _, Error title -> Error [title]
    | Error chapters, Ok _ -> Error chapters
    | Error chapters, Error title -> Error (title :: chapters)

