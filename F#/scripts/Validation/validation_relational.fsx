type Name = private Name of string
module Name =
    let from (x: string) =
        match x with
        | v when v = null || v.Length < 2 -> Error ["Crappy name"]
        | v -> Ok (Name v)

let apply fR xR =
    match fR, xR with
    | Ok f, Ok x -> Ok (f x)
    | Error e, Ok _ -> Error e
    | Ok _, Error e -> Error e
    | Error fe, Error xe -> Error (List.concat [fe; xe])

let (<!>) = Result.map
let (<*>) = apply

let invertResultList (xs: list<Result<'a, 'b list>>) = 
    xs
    |> List.fold (fun acc xr ->
        match xr with
        | Ok x ->
            {| Oks = x::acc.Oks; Errs = acc.Errs |}
        | Error e ->
            {| Oks = acc.Oks; Errs = List.concat [acc.Errs; e] |}
    ) {| Oks = []; Errs = [] |}
    |> fun res ->
        match res.Errs |> List.length with
        | 0 -> Ok (res.Oks)
        | _ -> Error (res.Errs)


type PersonDto = {
    Name: string
    Children: PersonDto list
}

type Person = {
    Name: Name
    Children: Person list
}

let rec ofDto (x: PersonDto): Result<Person, string list> =
    let fn (name: Name) (ps: Person list) = { Name = name; Children = ps}
    let name = Name.from x.Name
    let children = x.Children |> List.map ofDto |> invertResultList
    
    fn <!> name <*> children

let rec ofDtos (xs: PersonDto list) =
    xs |> List.map ofDto |> invertResultList


let dtos: PersonDto list = [
    { Name = "aa"; Children = [
        { Name = "aa1"; Children = [] }
        { Name = "aa"; Children = [] }
    ] }
    { Name = "aa"; Children = [] }
]

let vr = dtos |> ofDtos
vr |> printfn "VR is %A"